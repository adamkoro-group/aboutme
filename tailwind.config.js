module.exports = {
  darkMode: 'class',
  theme: {
    extend: {
      backgroundImage: {
        'home-pattern': "url('/img/bg.jpg')",
      }
    },
    colors: {
      'suse-text': '#FFFFFF',
      'suse-dark-green':'#19675b',
      'suse-dark-green-dark': '#0C322C',
      'suse-light-green': '#30BA78',
      'suse-light-green-dark': '#258e5b',
      'suse-info-text': '#FE7C3F',
    },

  }
  
}