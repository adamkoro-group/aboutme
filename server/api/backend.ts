
import type { IncomingMessage, ServerResponse } from 'http';
import dotenv from 'dotenv';

dotenv.config()

export default async (req: IncomingMessage, res: ServerResponse) => {
    res.statusCode = 200
    res.end(process.env.USER_ID)
}